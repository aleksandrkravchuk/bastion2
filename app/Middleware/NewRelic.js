'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class NewRelic {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({request}, next) {
    const newrelic = require('newrelic');
    newrelic.setControllerName(request.url(), request.method());
    // call next to advance the request
    await next();
  }
}

module.exports = NewRelic
